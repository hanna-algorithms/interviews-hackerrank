/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package days30.day14;

/**
 *
 * @author 985892
 */
public class Difference {
    private int[] elements;
    public int maximumDifference;
        
    // Add your code here
    public Difference(int[] elements) {
        this.elements = elements;
    }

    public void computeDifference() {
        int diff;
        for(int i=0; i < elements.length; i++) {
            for(int j=0; j < elements.length; j++) {
                if(i == j) continue;
                diff = Math.abs(elements[i] - elements[j]);
                if(diff > this.maximumDifference) {
                    this.maximumDifference = diff;
                } 
            }
        }
    }
    
}
