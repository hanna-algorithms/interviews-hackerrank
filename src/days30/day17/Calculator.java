/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package days30.day17;

/**
 *
 * @author 985892
 */
public class Calculator {
    
    class NegativeNumberException extends Exception {
        public NegativeNumberException(String message) {
            super(message);
        }
    }

    int power(int n, int p) throws NegativeNumberException {
        if(n < 0 || p < 0) {
            throw new NegativeNumberException("n and p should be non-negative");
        }
        return (int) Math.pow(n, p);
    }
}
