/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.warmup;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class MiniMax {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[5];
        for(int arr_i=0; arr_i < 5; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        long sum = 0;
        for(int i=0; i < arr.length; i++) {
            sum += arr[i]; 
        }

        long max = sum - arr[0];
        long min = max;
        long sum4;
        for(int i=1; i < arr.length; i++) {
            sum4 = sum - arr[i];
            if(sum4 > max) max = sum4;
            if(sum4 < min && sum4 > 0) min = sum4;
        }
        System.out.println(min + " " + max);
    }
}
