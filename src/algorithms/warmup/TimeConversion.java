/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.warmup;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class TimeConversion {
    static String timeConversion(String s) {
        // Complete this function
        String[] arr = s.split(":");
        int hour = Integer.parseInt(arr[0]);
        return String.valueOf(hour == 12 ? "00" : (hour + 12)) + ":" + arr[1] + ":"
                + String.valueOf(arr[2]).replace("AM", "").replace("PM", "");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = timeConversion(s);
        System.out.println(result);
    }
}
