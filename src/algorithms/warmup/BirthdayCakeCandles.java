/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.warmup;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class BirthdayCakeCandles {
    
    static int birthdayCakeCandles(int n, int[] ar) {
        int max = ar[0];
        for(int i=1; i < ar.length; i++) {
            if(ar[i] >= max) max = ar[i];
        }
        
        int c = 0;
        for(int i=0; i < ar.length; i++) {
            if(ar[i] == max) c++;
        }
        return c;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] ar = new int[n];
        for(int ar_i = 0; ar_i < n; ar_i++){
            ar[ar_i] = in.nextInt();
        }
        int result = birthdayCakeCandles(n, ar);
        System.out.println(result);
    }

}
