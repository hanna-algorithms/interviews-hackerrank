/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.dynamicProgramming;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class TheLongestCommonSubsequence {
    static int[][] T;
    
    static int[] longestCommonSubsequence(int[] a, int[] b) {
        Arrays.sort(a);
        
        // Complete this function
        // dynamic programming - memorized & recursive
        int l1 = a.length;
        int l2 = b.length;
        // fill table - 2 dimensional array
        T = new int[l1][l2];
        for(int i=0; i < l1; i++) 
            for(int j=0; j < l2; j++) 
                T[i][j] = -1;
        lcs(a, b, l1, l2, T);
        

        int k = 0;
        int[] c = new int[Math.max(l1, l2)];
        for(int i=0; i < l1; i++) 
            for(int j=0; j < l2; j++) 
                if(T[i][j] != -1)
                    c[k++] = T[i][j];
        return c;
    }
    
//        a b c - i
//      0 0 0 0
//    b 0 0 1 1
//j - c 0 0 1 2    
    static void lcs(int[] a, int[] b, int i, int j, int[][] T) {
        if(T[i][j] != -1) 
            return;
        if(i == 0 || j == 0) // base case
            return;
        if(a[i] == b[j]) // common subsequence
        {
            T[i][j] = a[i];
            lcs(a, b, i--, j--, T);
        }
        else
            lcs(a, b, i--, j, T);
            lcs(a, b, i, j--, T);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] a = new int[n];
        for(int a_i = 0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int[] b = new int[m];
        for(int b_i = 0; b_i < m; b_i++){
            b[b_i] = in.nextInt();
        }
        int[] result = longestCommonSubsequence(a, b);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
}
