/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class Quicksort1Partition {

    static int[] quickSort(int[] arr) {
        int p=arr[0];
        List<Integer> l = new ArrayList();
        List<Integer> e = new ArrayList();
        List<Integer> r = new ArrayList();
        int t;
        for(int i=0; i < arr.length; i++) {
            t = arr[i];
            if(t == p)
                e.add(t);
            else if(t < p)
                l.add(t);
            else
                r.add(t);
        }
        l.addAll(e);
        l.addAll(r);
        return l.stream().mapToInt(Integer::intValue).toArray();
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int[] result = quickSort(arr);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
}
