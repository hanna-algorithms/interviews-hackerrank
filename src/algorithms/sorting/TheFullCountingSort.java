/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class TheFullCountingSort {
    static void sort() {
        
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        
        List[] arr = new List[n]; // possible to use TreeMap - as keys are inserted in sorted order
        List<String> list;
        for(int a0 = 0; a0 < n; a0++){
            int x = in.nextInt();
            String s = in.next();
            list = (arr[x] == null) ? new ArrayList() : arr[x];   
            list.add((a0 < n/2) ? "-" : s); // replace with dash
            arr[x] = list;
        }

        for(List<String> l: arr) {
            l.forEach((elm) -> {
                System.out.print(elm + " ");
            });
        }
        in.close();
    }
}
