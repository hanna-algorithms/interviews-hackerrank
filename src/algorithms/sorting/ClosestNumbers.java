/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class ClosestNumbers {
    static int[] closestNumbers(int[] arr) {
        Arrays.sort(arr);
        
        List<Integer> list = new ArrayList();
        int tmad, mad = Math.abs(arr[0] - arr[1]); 
        for(int i=1; i < arr.length-1; i++) {
            tmad = Math.abs(arr[i] - arr[i+1]);
            if(tmad == mad) {
                list.add(arr[i]);
                list.add(arr[i+1]);
            } else if (tmad < mad) {
                mad = tmad;
                list.clear();
                list.add(arr[i]);
                list.add(arr[i+1]);
            }
        }
        return list.stream().mapToInt(Integer::intValue).toArray();
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        int[] result = closestNumbers(arr);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");


        in.close();
    }
    
}
