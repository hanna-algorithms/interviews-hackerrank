/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class CountingSort2 {

    static int[] countingSort(int[] arr) {
        int[] arr2 = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr2[arr[i]]++;
        }
        
        int k=0;
        int i=0;
        while(i < 100) {
            if (arr2[i] == 0) 
                i++;
            else {
                arr[k++] = i;
                arr2[i]--; 
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for (int arr_i = 0; arr_i < n; arr_i++) {
            arr[arr_i] = in.nextInt();
        }
        int[] result = countingSort(arr);
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + (i != result.length - 1 ? " " : ""));
        }
        System.out.println("");

        in.close();
    }
}
