/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class InsertionSortPart1 {
    static void insertionSort1(int n, int[] arr) {
        int v = arr[arr.length-1];
        for(int i=arr.length-2; i >= 0; i--) {
            if(arr[i] <= v) {
                arr[i+1] = v; 
                printArray(arr);
                break;
            }
            arr[i+1] = arr[i];
            printArray(arr);
            
            if(i == 0 && arr[i] > v) {
                arr[i] = v;
                printArray(arr);
            }
        }
    }
    
    static void printArray(int[] arr) {
        for(int i: arr) {
            System.out.print(arr[i] + ((arr.length-1 == i) ? "\n" : " "));
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        insertionSort1(n, arr);
        in.close();
    }
}
