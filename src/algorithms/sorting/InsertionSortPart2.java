/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.sorting;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class InsertionSortPart2 {
    
    static void insertionSort2(int n, int[] arr) {
        int t, k;
        for(int i=1; i < n; i++) {
            t = arr[i];
            k = i;
            for(int j=i-1; j >= 0; j--) {
                if (arr[j] <= t) break;
                arr[j+1] = arr[j]; // shift right
                k = j;
            }
            arr[k] = t;
            printArray(arr);
        }
    }

    static void printArray(int[] arr) {
        for(int i: arr) {
            System.out.print(arr[i] + ((arr.length-1 == i) ? "\n" : " "));
        }
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        insertionSort2(n, arr);
        in.close();
    }
}
