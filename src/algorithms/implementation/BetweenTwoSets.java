/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.implementation;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class BetweenTwoSets {
    
    // TODO
    static int[] gcd(int[] a) {
        return null;
    }
    static int[] lcm(int[] a) {
        return null;
    }
    
    static int getTotalX(int[] a, int[] b) {
        Arrays.sort(a);
        int maxA = a[a.length - 1]; // min value of x
        Arrays.sort(b);
        int minB = b[0]; // max value of x
        // max(a) <= x <= min(b)
        int c=0;
        outerloop:
        for(int x=maxA; x <= minB; x++) {
            for(int ai : a)
                if(x % ai != 0) continue outerloop;
            for(int bi : b)
                if(bi % x != 0) continue outerloop;
            c++;
        }
        return c;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] a = new int[n];
        for(int a_i = 0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int[] b = new int[m];
        for(int b_i = 0; b_i < m; b_i++){
            b[b_i] = in.nextInt();
        }
        int total = getTotalX(a, b);
        System.out.println(total);
        in.close();
    }
}
