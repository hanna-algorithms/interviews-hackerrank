/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms.implementation;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class Kangaroo {
    
    static String kangaroo(int x1, int v1, int x2, int v2) {
        if(v1 <= v2) // Given x1 < x2
            return "NO";

        // x1 + z*v1 == x2 + z*v2
        // z(v1 - v2) == x2 - x1
        // z == (x2 - x1)/(v1 - v2) where z is number of jumps
        if(((x2 - x1) % (v1 - v2)) == 0) 
            return "YES";
        else
            return "NO";
    }
    
    // Sample
    // v1=5 x1: 1 6 11 16 21 26 31 36 41 46
    // v2=2 x2: 3 5  7  9 11 13 15 17 19 21
    
    // Terminated due to timeout for Test Case #10: 43 2 70 2
    // v1=2 x1: 43 45 47
    // v2=2 x2: 70 72 74
    static String kangarooBruteForce(int x1, int v1, int x2, int v2) {
        if (v1 == v2)
            return "NO";

        while (!((x1 >= x2 && v1 > v2) || (x1 <= x2 && v1 < v2))) {
            x1 += v1; 
            x2 += v2; 
            if (x2 == x1)
                return "YES";
        }
        return "NO";
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x1 = in.nextInt();
        int v1 = in.nextInt();
        int x2 = in.nextInt();
        int v2 = in.nextInt();
        String result = kangaroo(x1, v1, x2, v2);
        System.out.println(result);
    }
}
