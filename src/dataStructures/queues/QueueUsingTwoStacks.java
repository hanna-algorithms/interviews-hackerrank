/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.queues;

import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Khandaa
 */
public class QueueUsingTwoStacks {
    public static void main(String[] args) {
        Stack s1 = new Stack();
        Stack s2 = new Stack();
        Scanner s = new Scanner(System.in);
        while(s.hasNext()) {
            switch(s.nextInt()) {
                case 1:
                    s1.push(s.nextInt());
                    break;
                case 2: 
                    if(s2.isEmpty()) {
                        while (!s1.isEmpty()) {
                            s2.push(s1.pop());
                        }
                    }
                    s2.pop();
                    break;
                case 3: 
                    if(s2.isEmpty()) {
                        while (!s1.isEmpty()) {
                            s2.push(s1.pop());
                        }
                    }
                    System.out.println(s2.peek());
                    break;
                default:;
            }
        }
    }
}
