/*^
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.stacks;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Khandaa
 */
public class BalancedBrackets {

    static String isBalanced(String s) {
        String result = "YES";
        Stack stack = new Stack(); // {[()]}
        char[] arr = s.toCharArray();
        String strC;
        for (char c : arr) {
            strC = String.valueOf(c);    
            switch (strC) {
                case "{":
                case "(":
                case "[":
                    stack.push(strC);
                    break;
                case "]":
                    if (stack.isEmpty())
                        result = "NO";
                    else if ("[".equals(stack.peek()))
                        stack.pop();
                    break;
                case ")":
                    if (stack.isEmpty())
                        result = "NO";
                    else if ("(".equals(stack.peek()))
                        stack.pop();
                    break;
                case "}":
                    if (stack.isEmpty())
                        result = "NO";
                    else if ("{".equals(stack.peek()))
                        stack.pop();
                    break;
                default:;
            }
        }
        result = "YES".equals(result) & stack.isEmpty() ? "YES" : "NO";
        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int a0 = 0; a0 < t; a0++) {
            String s = in.next();
            String result = isBalanced(s);
            System.out.println(result);
        }
        in.close();
    }
}
