/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.stacks;

import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Khandaa
 */
public class MaximumElement {
    
    public static void main(String[] args) {
        Stack<Integer> s = new Stack();
        Stack<Integer> sTmp = new Stack();
        
        int max=0; int t;
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            switch(scanner.nextInt()) {
                case 1:
                    t = scanner.nextInt();
                    if (t > max) max = t;
                    s.push(t);
                    break;
                case 2: 
                    if (s.isEmpty()) {
                        while (!sTmp.isEmpty()){
                            s.push(sTmp.pop());
                        }
                    }
                    t = s.pop();
                    if (t == max) {
                        max = 0;
                        while (!sTmp.isEmpty()){
                            s.push(sTmp.pop());
                        }
                        while(!s.isEmpty()) {
                            t = s.pop();
                            sTmp.push(t);
                            if (t > max) max = t;
                        }
                    }
                    break;
                case 3:
                    System.out.println(max);
                    break;
                default:;
            }
        }
        
    }
}
