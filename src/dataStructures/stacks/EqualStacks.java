/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.stacks;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Khandaa
 */
public class EqualStacks {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int n2 = in.nextInt();
        int n3 = in.nextInt();
        int h1[] = new int[n1];
        for (int h1_i = 0; h1_i < n1; h1_i++) {
            h1[h1_i] = in.nextInt();
        }
        int h2[] = new int[n2];
        for (int h2_i = 0; h2_i < n2; h2_i++) {
            h2[h2_i] = in.nextInt();
        }
        int h3[] = new int[n3];
        for (int h3_i = 0; h3_i < n3; h3_i++) {
            h3[h3_i] = in.nextInt();
        }
        // implementation
        // fill stacks
        Stack stack1 = new Stack();
        Stack stack2 = new Stack();
        Stack stack3 = new Stack();
        int height1 = 0, height2 = 0, height3 = 0;
        for (int i = n1 - 1; i >= 0; i--) {
            stack1.push(h1[i]);
            height1 += h1[i];
        }
        for (int i = n2-1; i >= 0; i--) {
            stack2.push(h2[i]);
            height2 += h2[i];
        }
        for (int i = n3-1; i >= 0; i--) {
            stack3.push(h3[i]);
            height3 += h3[i];
        }
        int nextHeight = height1;
        while (!stack1.isEmpty() && !stack2.isEmpty() && !stack3.isEmpty()) {
            while (height1 > nextHeight && !stack1.isEmpty()) {
                height1 -= (Integer) stack1.pop();
                if (height1 < nextHeight) nextHeight = height1;
            }
            while (height2 > nextHeight && !stack2.isEmpty()) {
                height2 -= (Integer) stack2.pop();
                if (height2 < nextHeight) nextHeight = height2;
            }
            while (height3 > nextHeight && !stack3.isEmpty()) {
                height3 -= (Integer) stack3.pop();
                if (height3 < nextHeight) nextHeight = height3;
            }
            if (nextHeight == height1 && height1 == height2 && height2 == height3) {
                System.out.println(height3);
                break;
            }
        }
    }
}
