/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.arrays;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author 985892
 */
public class P4LeftRotation {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int d = sc.nextInt();

        int rotatedIdx;
        int[] arr = new int[n];
        for(int i=0; i < n; i++) {
            rotatedIdx = n - d + i; // (n - d + i) % n
            rotatedIdx = rotatedIdx >= n ? Math.abs(rotatedIdx) - n : rotatedIdx; 
            arr[rotatedIdx] = sc.nextInt();
        }
//        for(int i=0; i < arr.length; i++) {
//            System.out.print(arr[i] + " ");
//        }
//        System.out.println(Arrays.asList(arr).stream()
//                .map(String::valueOf).collect(Collectors.joining(" ")));
        Arrays.asList(arr).stream().forEach(System.out::print);
    }
}
