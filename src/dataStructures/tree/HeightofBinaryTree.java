/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

/**
 *
 * @author Khandaa
 */
public class HeightofBinaryTree {
    class Node {
    	int data;
    	Node left;
    	Node right;
    }
    static int height(Node root) {
      	if (root == null) // leaf
            return -1;
        return 1 + Math.max(height(root.left), height(root.right));
    }

}
