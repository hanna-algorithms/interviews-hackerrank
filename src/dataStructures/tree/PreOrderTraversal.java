/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

/**
 *
 * @author Khandaa
 */
public class PreOrderTraversal {

    void preOrder(Node root) {
        if(root == null)
            return;
        System.out.print(root.data+" ");
        preOrder(root.left);
        preOrder(root.right);
    }
}

class Node {
    int data;
    Node left;
    Node right;
}

