/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

/**
 *
 * @author Khandaa
 */
public class InOrderTraversal {
    class Node {
        int data;
        Node left;
        Node right;
    }

    void inOrder(Node root) {
        if(root == null)
            return;
        inOrder(root.left);
        System.out.print(root.data+" ");
        inOrder(root.right);
    }
}



