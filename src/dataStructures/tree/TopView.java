/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.tree;

/**
 *
 * @author Khandaa
 */
public class TopView {
    class Node {
    	int data;
    	Node left;
    	Node right;
    }
    void topView(Node root) {
      	if (root == null) // leaf
            return;
        
        System.out.print(root.data+" ");
    }
}
