/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.linkedlists;

/**
 *
 * @author Khandaa
 */
public class InsertNodeAtSpecificPosition {
    class Node {
        int Data;
        Node Next;
    }
    
    Node InsertNth(Node head, int data, int position)
    {
        if(head == null) {
            head = new Node();
            head.Data = data;
        } else {
            Node currNode = head;
            Node newNode;
            int i = 0;
            while(currNode != null) {
                if (i++ == position) {
                   newNode = new Node();
                   newNode.Data = data;
                   newNode.Next = currNode.Next;
                   currNode.Next = newNode;
                   break;
                }
                currNode = currNode.Next;
            }
            
        }
        return head;
    }
}
