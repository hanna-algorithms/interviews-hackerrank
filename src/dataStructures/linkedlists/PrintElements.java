/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.linkedlists;

/**
 *
 * @author 985892
 */
public class PrintElements {
    
    public static void main(String[] args) {
        
    }

    class Node {
        int data;
        Node next;
    }
    
    void Print(Node head) {
        while (head != null) {
            System.out.println(head.data);
            head = head.next;
        }
    }
}
