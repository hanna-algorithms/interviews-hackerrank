/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.linkedlists;

/**
 *
 * @author 985892
 */
public class InsertNodeAtHead {
    
    class Node {
        int Data;
        Node Next;
    }
    
    Node Insert(Node head, int x) {
        if(head == null) {
            head = new Node();
            head.Data = x;
        } else {
            Node prevHead = head;
            head = new Node();
            head.Data = x;
            head.Next = prevHead;
        }
        return head;
    }
}
