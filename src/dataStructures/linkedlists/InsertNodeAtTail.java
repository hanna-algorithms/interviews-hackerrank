/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataStructures.linkedlists;

/**
 *
 * @author 985892
 */
public class InsertNodeAtTail {
    class Node {
        int data;
        Node next;
        
        
    }
    
    Node Insert(Node head, int data) {
        if (head == null) {
            head = new Node();
            head.data = data;
        } else if(head.next == null){
            head.next = new Node();
            head.next.data = data;
        } else {
            Insert(head.next, data);
        }
        return head;
    }
}
