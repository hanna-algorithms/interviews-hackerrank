/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap.introduction;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class EndofFile {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = "";
        int i=0;
        while(!line.contains("end-of-file")) {
            line = sc.nextLine();
            System.out.printf("%d %s%n", ++i, line);
        }
        sc.close();
    }
}
