/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap.introduction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class DateAndTime {
    
    public static String getDay(String day, String month, String year) {
        Calendar c = Calendar.getInstance();
        int y = Integer.parseInt(year);
        int m = Integer.parseInt(month);
        int d = Integer.parseInt(day);
        c.set(y, m, d); 
        SimpleDateFormat df = new SimpleDateFormat("EEEE");
        return df.format(c.get(Calendar.DAY_OF_WEEK)).toUpperCase();
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String month = in.next();
        String day = in.next();
        String year = in.next();
        
        System.out.println(getDay(day, month, year));
    }
}
