/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap.introduction;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class DataTypes {

    public static void main(String[] argh) {
        
        System.out.println("Primitive Data Types");
        System.out.printf("%s to %s | %s to %s%n", -Math.pow(2, 7), Math.pow(2, 7)-1, 0, Math.pow(2, 8)-1);
        System.out.printf("%s to %s | %s to %s%n", -Math.pow(2, 15), Math.pow(2, 15)-1, 0, Math.pow(2, 16)-1);
        System.out.printf("%s to %s | %s to %s%n", -Math.pow(2, 31), Math.pow(2, 31)-1, 0, Math.pow(2, 32)-1);
        System.out.printf("%s to %s | %s to %s%n", -Math.pow(2, 63), Math.pow(2, 63)-1, 0, Math.pow(2, 64)-1);
     
        System.out.println("Integer Literals");
        // The number 26, in decimal
        int decVal = 26;
        //  The number 26, in hexadecimal
        int hexVal = 0x1a;
        // The number 26, in binary
        int binVal = 0b11010;
        System.out.println(decVal);
        System.out.println(String.valueOf(hexVal));
        System.out.println(String.valueOf(binVal));
        
        System.out.println("Floating-Point Literals");
        double d1 = 123.4;
        // same value as d1, but in scientific notation
        double d2 = 1.234e2;
        float f1  = 123.4f;
        System.out.println(d1);
        System.out.println(d2);
        System.out.println(f1);
        
        System.out.println("Using Underscore Characters in Numeric Literals");
        long creditCardNumber = 1234_5678_9012_3456L;
        long socialSecurityNumber = 999_99_9999L;
        float pi =  3.14_15F;
        long hexBytes = 0xFF_EC_DE_5E;
        long hexWords = 0xCAFE_BABE;
        long maxLong = 0x7fff_ffff_ffff_ffffL;
        byte nybbles = 0b0010_0101;
        long bytes = 0b11010010_01101001_10010100_10010010;
        
        System.out.println(creditCardNumber);
        System.out.println(socialSecurityNumber);
        System.out.println(pi);
        System.out.println(hexBytes);
        System.out.println(hexWords);
        System.out.println(maxLong);
        System.out.println(nybbles);
        System.out.println(bytes);

        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            try {
                long x = sc.nextLong();
                System.out.println(x + " can be fitted in:");
                if (x >= -128 && x <= 127) {
                    System.out.println("* byte");
                }
                if (x >= -1 * Math.pow(2, 15) && x <= Math.pow(2, 15) - 1) {
                    System.out.println("* short");
                }
                if (x >= -1 * Math.pow(2, 31) && x <= Math.pow(2, 31) - 1) {
                    System.out.println("* int");
                }
                if (x >= -1 * Math.pow(2, 63) && x <= Math.pow(2, 63) - 1) {
                    System.out.println("* long");
                }
            } catch (Exception e) {
                System.out.println(sc.next() + " can't be fitted anywhere.");
            }

        }
    }
}
