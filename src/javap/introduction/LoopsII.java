/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap.introduction;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class LoopsII {

    public static void main(String[] argh) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            int subsum;
            for (int j = 0; j < n; j++) {
                subsum = 0;
                for (int k = 0; k <= j; k++) {
                    subsum += Math.pow(2, k) * b;
                }
                System.out.print(a + subsum + " ");
            }
            System.out.println("");
        }
        in.close();
    }
}
