/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap.bignumber;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class Bigdecimal {

    public static void main(String[] args) {
        //Input
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] s = new String[n + 2];
        for (int i = 0; i < n; i++) {
            s[i] = sc.next();
        }
        sc.close();

        BigDecimal[] b = new BigDecimal[n];
        for (int j = 0; j < s.length; j++) {
            if(s[j] != null)
                b[j] = new BigDecimal(s[j]);
        }
        Arrays.sort(b);
        
        for (int j = b.length - 1; j >= 0; j--) {
            System.out.println(b[j]);
        }
            
        // TODO: keep the format
    }
}
