/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javap;

import java.util.Scanner;

/**
 *
 * @author Khandaa
 */
public class Test {
    
    public static void main(String[] args) {
             long n = 461012;
      System.out.format("%d%n", n);      //  -->  "461012"
      System.out.format("%08d%n", n);    //  -->  "00461012"
      System.out.format("%+d%n", n);    //  -->  " +461012"
      System.out.format("%,d%n", n);    // -->  " 461,012"
      System.out.format("%+,d%n%n", n); //  -->  "+461,012"
      
            Scanner sc=new Scanner(System.in);
            System.out.println("================================");
            for(int i=0;i<3;i++){
                String s1 = sc.next();
                int x = sc.nextInt();
                System.out.printf("%-15s%03d%n", s1, x);
            }
            System.out.println("================================");
    }
}
