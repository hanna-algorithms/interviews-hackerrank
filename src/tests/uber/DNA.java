/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.uber;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Khandaa
 */
public class DNA {

    public static void main(String[] args) {
        System.out.println(dnaByHanna("AACGTCCCGTTTAAGGGGAGTGGAGTGGTTCCAGTAAACTAATTGGGGCGTAGTCCCCGGTTAAGGTTAATTCCAAAAGGCCGGAAACTTTTTACTCCCCAATTAACCAA"));
    }

    public static String dnaByGB(String s) {
        HashMap<String, Integer> dic = new HashMap<String, Integer>();
        dic.put("ACT", -1);
        dic.put("AGT", -1);
        dic.put("CGT", -1);

        int low = 1;
        int high = 1;
        int lo;
        int hi;

        String temp;
        StringBuilder sb = new StringBuilder();

        for (int i = 1; i < s.length() - 1; i++) {
            sb.append(s.charAt(i - 1));
            sb.append(s.charAt(i));
            sb.append(s.charAt(i + 1));
            temp = sb.toString();
            if (dic.containsKey(temp)) {
                dic.put(temp, i);
                if (!dic.containsValue(-1)) {
                    lo = dic.get("ACT");
                    hi = lo;
                    for (int val : dic.values()) {
                        lo = Math.min(lo, val);
                        hi = Math.max(hi, val);
                    }

                    if (high == 1 || (high - low > hi - lo)) {
                        high = hi;
                        low = lo;
                    }
                }
            }
            sb.setLength(0);
        }

        System.out.println(low + ", " + high);
        return dic.containsValue(-1) ? "" : s.substring(low - 1, high + 2);
    }

    public static String dnaByHanna(String dna) {
        String s1 = "ACT";
        String s2 = "AGT";
        String s3 = "CGT";

        // permutations
        Pattern p1 = Pattern.compile(s1 + "\\w*" + s2 + "\\w*" + s3);
        Pattern p2 = Pattern.compile(s1 + "\\w*" + s3 + "\\w*" + s2);

        Pattern p3 = Pattern.compile(s2 + "\\w*" + s3 + "\\w*" + s1);
        Pattern p4 = Pattern.compile(s2 + "\\w*" + s1 + "\\w*" + s3);

        Pattern p5 = Pattern.compile(s3 + "\\w*" + s1 + "\\w*" + s2);
        Pattern p6 = Pattern.compile(s3 + "\\w*" + s2 + "\\w*" + s1);

        // pattern matcher
        String str = "";
        Matcher m = p1.matcher(dna);
        if (m.find()) {
            str = m.group();
        }
        m = p2.matcher(dna);
        if (str.isEmpty() && m.find()) {
            str = m.group();
        }
        m = p3.matcher(dna);
        if (str.isEmpty() && m.find()) {
            str = m.group();
        }
        m = p4.matcher(dna);
        if (str.isEmpty() && m.find()) {
            str = m.group();
        }
        m = p5.matcher(dna);
        if (str.isEmpty() && m.find()) {
            str = m.group();
        }
        m = p6.matcher(dna);
        if (str.isEmpty() && m.find()) {
            str = m.group();
        }

        return str;
    }

}
