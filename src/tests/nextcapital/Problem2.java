/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.nextcapital;

/**
 *
 * @author Khandaa
 */
public class Problem2 {

    public static void main(String args[]) {
        String[] arr = new String[]{"GoCardinals", "Doge", "nExTcapITalxnextcapital", "Three5Three"};
        for (int i = 0; i < arr.length; i++) {
            arr[i] = strengthenPassword(arr[i]);
            System.out.println(arr[i]);
        }
    }

    static String strengthenPassword(String password) {
        // rule 1
        password = password.replaceAll("s|S", "5");
        // rule 2
        int length = password.length();
        if (length % 2 == 1) {
            int midPos = (length - 1) / 2;
            String midChar = password.substring(midPos, midPos + 1);
            if (isNumeric(midChar)) {
                password = String.format("%s%d%s", password.substring(0, midPos),
                        Long.parseLong(midChar) * 2,
                        password.substring(midPos + 1, length));
            }
        } else {
            // rule 3
            password = String.format("%s%s%s", swapCase(password.substring(length - 1, length)),
                    password.substring(1, length - 1),
                    swapCase(password.substring(0, 1)));
        }
        // rule 4
        if(password.toLowerCase().contains("nextcapital")) {
            password = password.replaceFirst("next", "NEXT");  // capitalize
            password = (new StringBuilder()).append(password).reverse().toString(); // reverse
        }
        return password;
    }

    static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    static char swapCase(String s) {
        char c = s.charAt(0);
        return Character.isUpperCase(c)
                ? Character.toLowerCase(c)
                : Character.toUpperCase(c);
    }

}
