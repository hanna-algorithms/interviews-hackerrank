/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests.nextcapital;

/**
 *
 * @author Khandaa
 */
public class Problem4 {
    
    public static void main(String args[]) {
        int[][] floristIntervals = {{1,10},{2,8},{3,6},{1,6}};
        System.out.println(maxFlorists(9, floristIntervals));
    }
    
    static int maxFlorists(int pathLength, int[][] floristIntervals) {
        int[][] path = new int[3][pathLength]; 
        int nbFlorist=0;
        
        // fill with -1 indicating empty paths
        for(int r=0; r < 3; r++) {
            for(int i=0; i < pathLength; i++) {
                path[r][i] = -1;
            }    
        }

        int begin, end, row;
        for(int i=0; i < floristIntervals.length; i++) {
            begin = floristIntervals[i][0];
            end = floristIntervals[i][1];
            if(end > pathLength) end = pathLength;
            
            row = doesFit(path, begin, end);
            if(row != -1) { // fill flowers
                for(int j=begin; j < end; i++) 
                    path[row][j] = 1;
                nbFlorist++;
            }
        }
        return nbFlorist;
    }
    
    static int doesFit(int[][] path, int begin, int end) {
        for(int r=0; r < 3; r++) {
            for(int i=begin; i < end; i++)
                if(path[r][i] != -1)
                    break;
            return r;
        }
        return -1;
    }


}
