import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Concordance {

    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
        String input = args.length > 0 ? args[0] : "./input.txt";
        String output = args.length > 1 ? args[1] : "./output.html";

        // for sorting & storing both key(words) & value(frequencies)
        Map<String, List<Integer>> concordance = new TreeMap();

        try (BufferedReader br = Files.newBufferedReader(Paths.get(input), Charset.forName("ISO-8859-1"))) {
            AtomicInteger cc = new AtomicInteger(0);    // for counting sentences
            String regex = "(?<!\\b\\p{Upper}\\w{0,4})(?=[.?!]\\s*\\p{Upper})[.?!]\\s*";
            br.lines()
                    .flatMap(Pattern.compile(regex)::splitAsStream) // split by sentences
                    .forEach((sentence) -> {
                        cc.getAndIncrement();
                        Pattern.compile("\\s").splitAsStream(sentence) // split by words (or use \\w)
                                .forEach((word) -> {
                                    word = word.replaceAll(",", "").toLowerCase();  // or use \\W - non-word characters
                                    List<Integer> senNbr = concordance.containsKey(word) // indicate sentence numbers
                                            ? concordance.get(word) : new ArrayList();
                                    senNbr.add(cc.get());
                                    concordance.put(word, senNbr);
                                });
                    });

            // OUTPUT
            try (PrintWriter out = new PrintWriter(output)) {
                out.println("<style>" // apply style to the output
                        + "ol{list-style-type:lower-alpha; column-count:2;}"
                        + "span{width:30%; display:inline-block;}"
                        + "span.freq{width:68%; display:inline-block; overflow:hidden;}"
                        + "</style>");
                out.println("<ol>");    // use ordered list for numbering with letters
                concordance.keySet().forEach((word) -> {
                    out.printf("<li><span>%s</span><span class='freq'>{%d:%s}</span></li>", word,
                            concordance.get(word).size(),
                            concordance.get(word).toString().replaceAll("\\[|\\]|\\s", ""));
                });
                out.println("</ol>");
                out.close();

                // launch the output file
                Runtime.getRuntime().exec(new String[]{"cmd.exe", "/C", new File(output).getAbsolutePath()});
                System.out.println("Running time (msec):" + (System.currentTimeMillis() - begin));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Concordance.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException e) {
            Logger.getLogger(Concordance.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
