1. To execute the program using Command Prompt: (You will need Java runtime environment)
	a. Start Command Prompt.
	b. Copy Concordance.java to C: drive and type the following command
	c. C:\> java Concordance
	   or 
	   C:\> java Concordance /path/to/inputfile /path/to/outputfile
	   i.e. java Concordance c:\\khandaabat\\data.txt c:\\khandaabat\\result.html
	
2. To compile the source code(Concordance.java): 
	a. Start Command Prompt.
    b. Copy Concordance.java to C: drive and type the following command
	c. C:\> javac Concordance.java
	d. C:\> java Concordance [args...]
	
3. To build executable jar file in Command Prompt:
	a. Start Command Prompt.
    b. Copy Concordance.java to C: drive and type the following command
    d. Set path to include JDK’s bin. For example: C:\> path c:\Program Files\Java\jdk1.8.0_131\bin;%path%
    e. Compile the class
		C:\> javac *.java
	f. Create a manifest file and jar file:
		C:\> jar cvfe Concordance.jar Concordance *.class
	g. Test the jar:
		C:\> Concordance.jar
		or
		C:\> java -jar Concordance.jar
		or 
		C:\> java -jar Concordance.jar /path/to/inputfile /path/to/outputfile
		
Solution approach: 
The program reads content of input file as stream and splits by sentences. Then split each sentence by words while locating sentence numbers that correspond to words.
Used TreeMap to store words to be sorted and ArrayList to store frequences as value of the map. Used lambda expressions for implementing logics.
It prints result into html file with formating and launches the file.