package tests.bridgewater;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Concordance {

    public static void main(String args[]) {
        String input = args.length > 0 ? args[0] : "c:\\khandaabat\\input.txt";
        String output = args.length > 1 ? args[1] : "c:\\khandaabat\\output.html";

        Map<String, List<Integer>> concordance = new TreeMap();         // for sorting & storing both key & vals

        try (BufferedReader br = Files.newBufferedReader(Paths.get(input))) {
            AtomicInteger cc = new AtomicInteger(0);                    // for counting sentences
            String regex = "(?<!\\b\\p{Upper}\\w{0,4})(?=[.?!]\\s*\\p{Upper})[.?!]\\s*";
            br.lines()
                    .flatMap(Pattern.compile(regex)::splitAsStream)     // split by sentences
                    .forEach((sentence) -> {
                        cc.getAndIncrement();
                        Pattern.compile("\\s").splitAsStream(sentence)  // split by words (or use \\w)
                                .forEach((word) -> {
                                    word = word.replaceAll(",", "").toLowerCase(); // or use \\W - non-word characters
                                    List<Integer> senNbr = concordance.containsKey(word) // sentence numbers
                                            ? concordance.get(word) : new ArrayList();
                                    senNbr.add(cc.get());
                                    concordance.put(word, senNbr);
                                });
                    });

            // OUTPUT
            try (PrintWriter out = new PrintWriter(output)) {
                out.println("<style>" // apply style to the output
                        + "ol{list-style-type:lower-alpha; column-count:2;}"
                        + "span{width:40%; display:inline-block;}"
                        + "</style>");
                out.println("<ol>");                            // use ordered list for numbering with letters
                concordance.keySet().forEach((word) -> {
                    out.printf("<li><span>%s</span>{%d:%s}</li>", word,
                            concordance.get(word).size(),
                            concordance.get(word).toString().replaceAll("\\[|\\]|\\s", ""));
                });
                out.println("</ol>");
                out.close();

                Runtime.getRuntime().exec(new String[]{"cmd.exe", "/C", output}); // launch the output file
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Concordance.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (IOException e) {
            Logger.getLogger(Concordance.class.getName()).log(Level.SEVERE, null, e);
        }

    }
}