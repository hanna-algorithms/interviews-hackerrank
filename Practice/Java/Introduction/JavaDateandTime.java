import java.time.LocalDate;

public class Solution {
    
    public static String getDay(String d, String m, String y) {
        LocalDate dt = LocalDate.of(Integer.valueOf(y), Integer.valueOf(m), Integer.valueOf(d));
        return String.valueOf(dt.getDayOfWeek());
    }
}