import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        String[] ss = s.split("[^a-zA-Z\\d:]");
        
        int c=0;
        for(String a : ss)
            if(!"".equals(a.trim())) c++;
        System.out.println(c);
        
        for(String a : ss)
            if(!"".equals(a.trim())) System.out.println(a);
        scan.close();
    }
}

