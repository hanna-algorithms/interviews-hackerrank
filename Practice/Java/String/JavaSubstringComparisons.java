import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
  
    public static String getSmallestAndLargest(String s, int k) {     
        ArrayList l = new ArrayList<String>();
        for(int i=0; i < s.length() - k + 1; i++) {
            l.add(s.substring(i, i + k));
        }
        Collections.sort(l);
        return l.get(0) + "\n" + l.get(l.size() - 1);
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();
      
        System.out.println(getSmallestAndLargest(s, k));
    }
}
