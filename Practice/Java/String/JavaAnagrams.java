    static boolean isAnagram(String a, String b) {
        int[] arr = new int[26];

        for(char c : a.toLowerCase().toCharArray())
            arr[c - 'a']++;
        
        for(char c : b.toLowerCase().toCharArray())
            arr[c - 'a']--;
                
        for(int i : arr)
            if(i != 0) return false;

        return  true;
    }