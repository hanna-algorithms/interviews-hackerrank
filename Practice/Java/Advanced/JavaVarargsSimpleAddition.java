class Add {
    public void add(Integer n1, Integer n2) {
        System.out.println(String.format("%d+%d=%d", n1, n2, (n1+n2)));
    }
    public void add(Integer n1, Integer n2, Integer n3) {
        System.out.println(String.format("%d+%d+%d=%d", n1, n2, n3, (n1+n2+n3)));
    }
    public void add(Integer n1, Integer n2, Integer n3, Integer n4) {
        System.out.println(String.format("%d+%d+%d+%d=%d", n1, n2, n3, n4, (n1+n2+n3+n4)));
    }
    public void add(Integer n1, Integer n2, Integer n3, Integer n4, Integer n5) {
        System.out.println(String.format("%d+%d+%d+%d+%d=%d", n1, n2, n3, n4, n5, (n1+n2+n3+n4+n5)));
    }
    public void add(Integer n1, Integer n2, Integer n3, Integer n4, Integer n5, Integer n6) {
        System.out.println(String.format("%d+%d+%d+%d+%d+%d=%d", n1, n2, n3, n4, n5, n6, (n1+n2+n3+n4+n5+n6)));
    }
}